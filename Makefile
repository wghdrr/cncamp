all: build

object=httpserver

build:
	go env -w GO111MODULE=on && go env -w GOPROXY=https://goproxy.cn,direct  && CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -mod=mod -o $(object) httpserver.go

.PHONY: clean
clean:
	rm httpserver 