FROM golang:1.17.5 

COPY httpserver /

EXPOSE 8080
CMD [ "/httpserver" ]